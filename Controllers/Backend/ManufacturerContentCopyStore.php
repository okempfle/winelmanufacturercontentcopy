<?php

class Shopware_Controllers_Backend_ManufacturerContentCopyStore extends Shopware_Controllers_Backend_Application
{
    protected $model = 'WinelManufacturerContentCopy\Models\Store\ManufacturerContentCopyStore';

    public function getBaseFieldsAction()
    {
        $data = [
            ['id' => 1, 'name' => 'name'],
            ['id' => 2, 'name' => 'link'],
            ['id' => 3, 'name' => 'description'],
            ['id' => 4, 'name' => 'metaTitle'],
            ['id' => 5, 'name' => 'metaDescription']
        ];

        $this->view->assign([
            'data' => $data,
            'total' => count($data),
        ]);
    }

    public function getShopsAction()
    {
        $shops  = Shopware()->Db()->fetchAll(
            'SELECT id,name 
            FROM s_core_shops sc
            WHERE sc.active = ? ',[1] ) ;
        $data=[];
        foreach($shops as $shop){

            $data[]=$shop;
        }


        $this->view->assign([
            'data' => $data,
            'total' => count($data),
        ]);
    }
    public function getAttributeFieldsAction()
    {
        $shops  = Shopware()->Db()->fetchAll(
            'SELECT id,column_name as name
            FROM s_attribute_configuration asa
            WHERE asa.table_name = ? ',['s_articles_supplier_attributes'] ) ;
        $data=[];
        foreach($shops as $shop){

            $data[]=$shop;
        }


        $this->view->assign([
            'data' => $data,
            'total' => count($data),
        ]);
    }
    public function getCatBaseFieldsAction()
    {
        $data = [
            ['id' => 1, 'name' => 'description'],
            ['id' => 2, 'name' => 'cmsheadline'],
            ['id' => 3, 'name' => 'cmstext'],
            ['id' => 4, 'name' => 'meta_title'],
            ['id' => 5, 'name' => 'metadescription'],
            ['id' => 6, 'name' => 'stream_id'],
            ['id' => 7, 'name' => 'external'],
            ['id' => 8, 'name' => 'template']
        ];

        $this->view->assign([
            'data' => $data,
            'total' => count($data),
        ]);
    }
    public function getCatAttributeFieldsAction()
    {
        $shops  = Shopware()->Db()->fetchAll(
            'SELECT id,column_name as name
            FROM s_attribute_configuration asa
            WHERE asa.table_name = ? ',['s_categories_attributes'] ) ;
        $data=[];
        foreach($shops as $shop){

            $data[]=$shop;
        }


        $this->view->assign([
            'data' => $data,
            'total' => count($data),
        ]);
    }
}