<?php

namespace WinelManufacturerContentCopy\Subscriber;

use Enlight\Event\SubscriberInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

class BackendSubscriber implements SubscriberInterface
{
    private $container;

    public function __construct(ContainerInterface $container)
    {
        $this->container = $container;
    }

    private function getPluginPath()
    {
        return $this->container->getParameter('winel_manufacturer_content_copy.plugin_dir');
    }
    private function getPluginId()
    {
        return $this->container->getParameter('winel_order_calculation_models.plugin_id');
    }

    public static function getSubscribedEvents()
    {
        return [
            'Enlight_Controller_Dispatcher_ControllerPath_Backend_ManufacturerContentCopyStore' => 'onManufacturerContentCopyStore'
        ];
    }

    public function onManufacturerContentCopyStore()
    {
        return $this->getPluginPath() . '/Controllers/Backend/ManufacturerContentCopyStore.php';
    }


}