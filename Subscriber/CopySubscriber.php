<?php

namespace WinelManufacturerContentCopy\Subscriber;

use Enlight\Event\SubscriberInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

class CopySubscriber implements SubscriberInterface
{
    private $container;
    private $manufacturers;

    public function __construct(ContainerInterface $container)
    {
        $this->container = $container;
    }

    private function getPluginPath()
    {
        return $this->container->getParameter('winel_manufacturer_content_copy.plugin_dir');
    }
    private function getPluginId()
    {
        $cronjob  = Shopware()->Db()->fetchAll(
            'SELECT id
            FROM s_core_plugins scp
            WHERE scp.name = ? ',['WinelManufacturerContentCopy'] ) ;
        return $cronjob[0]['id'];
    }

    public static function getSubscribedEvents()
    {
        return [
            'Shopware_CronJob_WinelManufacturerContentCopyCron' => 'WinelManufacturerContentCopyRun'
        ];
    }

    public function WinelManufacturerContentCopyRun()
    {
        $shops=$this->getShops();
        $this->manufacturers=$this->getManufacturer();
        foreach ($shops as $shop){
            $shopId=$shop['id'];
            $shopConfig=$this->getShopPluginConfig($shopId);
            if($shopConfig['copy']){
                $masterShopName=$shopConfig['master'];
                $masterShopId=$this->getShopByName($masterShopName);
                $baseFieldsArray=$shopConfig['baseFields'];
                $attributeFieldsArray=$shopConfig['attributeFields'];
                $this->copyManufacturer($shopId,$masterShopId,$baseFieldsArray,$attributeFieldsArray);
            }
        }
    }

    public function copyManufacturer($shopId,$masterShopId,$baseFieldsArray,$attributeFieldsArray){
        foreach($this->manufacturers as $manufacturer) {
            $manufacturerId = $manufacturer['id'];
            $masterText = $this->getTranslations($manufacturerId, $masterShopId);
            if ($masterText != '') {
                $aktShopTextArray = $this->getTranslations($manufacturerId, $shopId);
                $newTextArray = $this->buildNewTextArray($masterText, $aktShopTextArray, $baseFieldsArray, $attributeFieldsArray);
                if($aktShopTextArray==''){
                    $this->saveTranslations($shopId,$manufacturerId,$newTextArray);
                }else{
                    $this->updateTranslations($shopId,$manufacturerId,$newTextArray);
                }
            }
        }
    }

    public function buildNewTextArray($masterText,$aktShopTextArray,$baseFieldsArray,$attributeFieldsArray){

        if($aktShopTextArray==''){
            $aktShopTextArray=[];
        }
        foreach($baseFieldsArray as $baseField){

           if(!isset($masterText[$baseField])){
                $aktShopTextArray[$baseField]='';
            }else{
                $aktShopTextArray[$baseField]=$masterText[$baseField];
            }

        }


        foreach($attributeFieldsArray as $attributeField){
            $attributeFieldName='__attribute_'.$attributeField;
            if(!isset($masterText[$attributeFieldName])){
                $aktShopTextArray[$attributeFieldName]='';
            }else{
                $aktShopTextArray[$attributeFieldName]=$masterText[$attributeFieldName];
            }
        }
        unset( $aktShopTextArray['name']);
 
        return $aktShopTextArray;
    }


    public function getMasterAttributes($manufacturerId){

        $attributes  = Shopware()->Db()->fetchAll(
            'SELECT *
            FROM s_articles_supplier_attributes sasa
            WHERE sasa.supplierID = ? ',[$manufacturerId] ) ;

        return $attributes;

    }

    public function getTranslations($manufacturerId,$shopId){

        $translation  = Shopware()->Db()->fetchRow(
            'SELECT objectdata
            FROM s_core_translations sct
            WHERE sct.objecttype = ? and sct.objectkey=? and sct.objectlanguage=? ',['supplier',$manufacturerId,$shopId] ) ;
        $translationData=$translation['objectdata'];
        if($translationData ==NULL || $translationData==''){
            return '';
        }else{
             return unserialize($translationData);
        }
    }

    public function saveTranslations($shopId,$manufacturerId,$shopText){
       $serializedText=serialize($shopText);

        $this->container->get('dbal_connection')->executeQuery('insert into s_core_translations (objecttype,objectkey,objectlanguage,objectdata) Values (?,?,?,?)', [
            'supplier', $manufacturerId, $shopId, $serializedText
        ]);

    }
    public function updateTranslations($shopId,$manufacturerId,$shopText){
        $serializedText=serialize($shopText);

        $this->container->get('dbal_connection')->executeQuery('update s_core_translations set objectdata =? where objecttype=? and objectkey=? and objectlanguage=? ', [
            $serializedText,'supplier', $manufacturerId, $shopId
        ]);

    }

    public function getShopByName($shopName){
        $shop  = Shopware()->Db()->fetchRow(
            'SELECT id
            FROM s_core_shops sc
            WHERE sc.name = ? ',[$shopName] ) ;
        return $shop['id'];
    }

    public function getShops(){
        $shops  = Shopware()->Db()->fetchAll(
            'SELECT id,name 
            FROM s_core_shops sc
            WHERE sc.active = ? ',[1] ) ;
        return $shops;

    }

    public function getManufacturer(){
        $manufacturer  = Shopware()->Db()->fetchAll(
            'SELECT * 
            FROM s_articles_supplier sas 
            ',[] ) ;
        return $manufacturer;

    }

    public function getShopPluginConfig($shopId){

        $pluginId=$this->getPluginId();
        $formId=$this->getFormId($pluginId);
        $formElements=$this->getFormElements($formId);
        $shopConfig=$this->getFormElementsValues($formElements,$shopId);
        return $shopConfig;

    }

    public function getFormId($pluginId){
        $forms  = Shopware()->Db()->fetchAll(
            'SELECT id,name 
            FROM s_core_config_forms ccf
            WHERE ccf.plugin_id = ? ',[$pluginId] ) ;
        return $forms[0]['id'];

    }

    public function getFormElements($formId){
        $formelEments  = Shopware()->Db()->fetchAll(
            'SELECT id,name 
            FROM s_core_config_elements cce
            WHERE cce.form_id = ? ',[$formId] ) ;
        return $formelEments;

    }

    public function getFormElementsValues($formElements,$shopId){
        $valueArray=[];
        foreach($formElements as $formElement){
            $elementId=$formElement['id'];
           $serializedValue=$this->getSerializedFormElementsValues($elementId,$shopId);
            $valueArray[$formElement['name']]=unserialize($serializedValue);

        }
        return $valueArray;

    }
    public function getSerializedFormElementsValues($elementId,$shopId){
        $value  = Shopware()->Db()->fetchRow(
            'SELECT id,value 
            FROM s_core_config_values ccv
            WHERE ccv.element_id = ?  and shop_id=?',[$elementId,$shopId] ) ;
        return $value['value'];

    }


}