<?php

namespace WinelManufacturerContentCopy;

use Shopware\Components\Plugin;
use Symfony\Component\DependencyInjection\ContainerBuilder;

class WinelManufacturerContentCopy extends Plugin
{
    public function build(ContainerBuilder $container)
    {
        $container->setParameter('winel_manufacturer_content_copy.plugin_dir', $this->getPath());
        $container->setParameter('winel_manufacturer_content_copy.plugin_name', $this->getName());
        parent::build($container);
    }
}